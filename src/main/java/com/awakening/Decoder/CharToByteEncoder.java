package com.awakening.Decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @Author:AwakeningCode
 * @Date: Created in 18:14 2018\3\8 0008
 */
public class CharToByteEncoder extends MessageToByteEncoder<Character>{
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext,
                          Character msg, ByteBuf out) throws Exception {
        //将Character解码为char，并将其写入到出站ByteBuf中
        out.writeChar(msg);
    }
}
