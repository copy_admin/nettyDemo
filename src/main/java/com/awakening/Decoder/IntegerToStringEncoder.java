package com.awakening.Decoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

/**
 * @Author:AwakeningCode
 * @Date: Created in 16:43 2018\3\8 0008
 */
public class IntegerToStringEncoder extends MessageToMessageEncoder<Integer>{
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext,
                          Integer msg, List<Object> out) throws Exception {
        //将Integer转换为String，并将其添加到List中
        out.add(String.valueOf(msg));
    }
}
