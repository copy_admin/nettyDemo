package com.awakening.Decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

/**
 * @Author:AwakeningCode
 * @Date: Created in 16:37 2018\3\8 0008
 */
public class ShortToByteEncoder extends MessageToByteEncoder<Short>{
    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext,
                          Short msg, ByteBuf out) throws Exception {
        //将Short写入ByteBuf中
        out.writeShort(msg);
    }
}
