package com.awakening.Decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @Author:AwakeningCode
 * @Date: Created in 18:09 2018\3\8 0008
 */
public class ByteToCharDecoder extends ByteToMessageDecoder{
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext,
                          ByteBuf in, List<Object> out) throws Exception {
        while (in.readableBytes() >= 2){
            //将一个或者多个Character对象添加到传出的List中
            out.add(in.readChar());
        }
    }
}
