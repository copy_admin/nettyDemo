package com.awakening.Decoder;

import io.netty.channel.CombinedChannelDuplexHandler;

/**
 * @Author:AwakeningCode
 * @Date: Created in 18:16 2018\3\8 0008
 */
public class CombinedByteCharCodec extends
        CombinedChannelDuplexHandler<ByteToCharDecoder,CharToByteEncoder>{
    //将委托实例传递给父类
    public CombinedByteCharCodec() {
        super(new ByteToCharDecoder(),new CharToByteEncoder());
    }
}
