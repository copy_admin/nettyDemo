package com.awakening.Decoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

/**
 * @Author:AwakeningCode
 * @Date: Created in 16:19 2018\3\8 0008
 */
public class IntegerToStringDecoder extends MessageToMessageDecoder<Integer>{
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext,
                          Integer msg, List<Object> out) throws Exception {
        //将Integer消息转换为它的String表示，并将其添加到输出的List中
        out.add(String.valueOf(msg));
    }
}
