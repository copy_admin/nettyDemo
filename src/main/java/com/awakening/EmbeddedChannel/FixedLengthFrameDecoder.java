package com.awakening.EmbeddedChannel;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @Author:AwakeningCode
 * @Date: Created in 13:52 2018\3\8 0008
 */
//扩展ByteToMessageDecoder以处理入站字节，并将它们解码为消息
public class FixedLengthFrameDecoder extends ByteToMessageDecoder{

    private final int frameLength;

    //指定要生成的帧的长度
    public FixedLengthFrameDecoder(int frameLength) throws IllegalAccessException {
        if (frameLength <= 0){
            throw new IllegalAccessException("frameLength must be a positive integer: " + frameLength);
        }
        this.frameLength = frameLength;
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf,
                          List<Object> list) throws Exception {
        //检查是否有足够的字节可以被读取，以生成下一个帧
        while (byteBuf.readableBytes() >= frameLength){
            //从ByteBuf中读取一个新帧
            ByteBuf buf = byteBuf.readBytes(frameLength);
            //将该帧添加到已被解码的消息列表中
            list.add(buf);
        }
    }
}
