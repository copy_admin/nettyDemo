package com.awakening.Transmission;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.util.CharsetUtil;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @Author:AwakeningCode
 * @Date: Created in 14:35 2018\2\25 0025
 */
public class Testing1 {

    public static void main(String[] args) {
        /*Channel channel = ...
        //创建持有要写数据的ByteBuf
        ByteBuf buf = Unpooled.copiedBuffer("your data", CharsetUtil.UTF_8);
        //写数据并冲刷它
        ChannelFuture cf = channel.writeAndFlush(buf);
        //添加ChannelFutureListener以便在写操作完成后接收通知
        cf.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture channelFuture) throws Exception {
                //写操作完成，并且没有错误发生
                if (channelFuture.isSuccess()){
                    System.out.println("Write successful");
                } else {
                    //记录错误
                    System.out.println("Write error");
                    channelFuture.cause().printStackTrace();
                }
            }
        });*/

        /*final Channel channel = ...
        //创建持有要写数据的ByteBuf
        final ByteBuf buf = Unpooled.copiedBuffer("your data",CharsetUtil.UTF_8).retain();
        //创建将数据写到Channel的Runable
        Runnable writer = new Runnable() {
            @Override
            public void run() {
                channel.writeAndFlush(buf.duplicate());
            }
        };
        //获取到线程池Executor的引用
        Executor executor = Executors.newCachedThreadPool();

        //write in one thread
        //递交写任务给线程池以便在某个线程中执行
        executor.execute(writer);

        //write in another thread
        //递交另一个写任务以便在另一个线程中执行
        executor.execute(writer);*/
    }

}
