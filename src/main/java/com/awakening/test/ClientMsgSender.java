package com.awakening.test;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @Author:AwakeningCode
 * @Date: Created in 20:18 2018\2\4 0004
 */
public class ClientMsgSender {

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

    private OutputStream out;

    public ClientMsgSender(OutputStream out) {
        this.out = out;
    }

    public void stop(){
        scheduler.shutdown();
    }

    public void start(){
        scheduler.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                String msg = new String("a1");
                if (msg!=null){
                    try {
                        out.write(msg.getBytes(Charset.forName("UTF-8")));
                    }catch (IOException e){
                        e.printStackTrace();
                    }
                }
            }
        },3,3, TimeUnit.SECONDS);
    }
}
