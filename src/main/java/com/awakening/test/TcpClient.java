package com.awakening.test;

import com.awakening.message.Header;
import com.awakening.message.Message;
import io.netty.util.CharsetUtil;

import java.io.*;
import java.net.Socket;

/**
 * @Author:AwakeningCode
 * @Date: Created in 20:20 2018\2\4 0004
 */
public class TcpClient {

    private final static String serverString = "127.0.0.1";
    //private final static String serverString = "39.106.105.3";
    //private final static String serverString = "172.17.43.141";
    private final static int serverPort = 18866;

    public static void main(String[] args) {
        Socket socket = null;
        try {
            socket = new Socket(serverString,serverPort);
            System.out.println("Connected to server...send echo string (quit to end)");

            final InputStream in = socket.getInputStream();
            OutputStream out = socket.getOutputStream();

            startReceiveThread(in);

            sendMsgToServerFromThread(out);

        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    private static void startReceiveThread(final InputStream in) {
        Thread receiveThread = new Thread() {
            public void run() {
                while (true) {
                    byte[] readBytes = new byte[2048];
                    int ret = 0;
                    try {
                        ret = in.read(readBytes);
                    } catch (IOException e) {
                        break;
                    }
                    if (ret == -1) {
                        break;
                    }
                    System.out.println(ret);
                    byte[] retBytes = new byte[ret-5];
                    for (int i = 0; i < ret-5 ; i++){
                        retBytes[i] = readBytes[i+5];
                    }
                    String retStrings = new String(retBytes);
                    System.out.println("Received : " + retStrings);

                    retStrings = new String(readBytes);
                    System.out.println("Received : " + retStrings);
                }
            }
        };

        receiveThread.setDaemon(true);
        receiveThread.start();
    }

    private static void sendMsgToServerFromInput(OutputStream out) {
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(
                System.in));
        while (true) {
            String msg = new String();
            try {
                msg = inFromUser.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (msg.equals("quit")) {
                break;
            }
            byte[] msgBytes = getMessageBytes(msg);
            if (msgBytes != null) {
                try {
                    out.write(msgBytes);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void sendMsgToServerFromThread(final OutputStream out) {
        ClientMsgSender msgSender = new ClientMsgSender(out);
        msgSender.start();
    }

    public static byte[] getMessageBytes(String msg) {
        msg = msg.trim();
        if (!msg.isEmpty()) {
            byte[] data = msg.getBytes();

            Header header = new Header();
            byte msgType = new Byte("2");
            header.setMsgType(msgType);
            header.setMsgLength(5 + data.length);

            Message message = new Message();
            try {
                message.setData(msg.getBytes("UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            message.setHeader(header);
            return message.getBytes();
        }
        return null;
    }
}
