package com.awakening.demo;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.ssl.SslHandler;

import javax.net.ssl.SSLEngine;

/**
 * @Author:AwakeningCode
 * @Date: Created in 21:59 2018\3\10 0010
 */
/*public class ApnsClientPipelineInitializer extends ChannelInitializer<Channel>{
    private final SSLEngine clientEngine;

    public ApnsClientPipelineInitializer(SSLEngine clientEngine) {
        //一个X.509认证的请求需要一个javax.net.ssl.SSLEngine类的实例
        this.clientEngine = clientEngine;
    }

    @Override
    protected void initChannel(Channel channel) throws Exception {
        final ChannelPipeline pipeline = channel.pipeline();
        //构造一个Netty的SslHandler
        final SslHandler handler = new SslHandler(clientEngine);
        //APNS将尝试在连接后不久重新协商SSL，需要允许重新协商
        //handler.setEnableRenegotiation(true);
        pipeline.addLast("ssl",handler);
        //这个类扩展了Netty的ByteToMessageDecoder，并且处理了APNS返回一个错误代码并断开连接的情况
        pipeline.addLast("decoder",new ApnsResponseDecoder());
    }
}*/
