package com.awakening.demo;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteOrder;

/**
 * @Author:AwakeningCode
 * @Date: Created in 21:34 2018\3\10 0010
 */
public final class ApnsMessage {
    //APNS消息总是以一个字节大小的命令作为开始，因此该值被编码为常量
    private static final byte COMMAND = (byte)1;
    /*public ByteBuf toBuffer(){
        //因为消息的大小不一，所以出于效率考虑，在ByteBuf创建之前将先计算它
        short size = (short)(1 +//Command
            4 + //Identifier
            4 + //Expiry
            2 + //DT length header
            32 + //DS length
            2 + //body length header
            body.length);
        //在创建时，ByteBuf的大小正好，并且指定了用于APNS的大端字节序
        ByteBuf buf = Unpooled.buffer(size).order(ByteOrder.BIG_ENDIAN);
        buf.writeByte(COMMAND);
        //来自于类中其它地方维护的状态的各种值将会被写入到缓冲区中
        buf.writeInt(identifier);
        buf.writeInt(expiryTime);
        //这个类中的deviceToken字段是一个Java的byte[]
        buf.writeShort((short)deviceToken.length);
        buf.writeBytes(deviceToken);
        buf.writeShort((short)body.length);
        buf.writeBytes(body);
        //当缓冲区已经就绪时，简单地将它返回
        return buf;
    }*/
}
