package com.awakening.ChannelHandler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;

/**
 * @Author:AwakeningCode
 * @Date: Created in 14:33 2018\3\6 0006
 */
@ChannelHandler.Sharable
//扩展了ChannelInboundHandlerAdapter
public class DiscardHandler extends ChannelInboundHandlerAdapter{

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //丢弃已接收的消息
        ReferenceCountUtil.release(msg);
    }
}
