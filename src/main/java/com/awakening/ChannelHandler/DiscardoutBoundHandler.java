package com.awakening.ChannelHandler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.util.ReferenceCountUtil;

/**
 * @Author:AwakeningCode
 * @Date: Created in 15:09 2018\3\6 0006
 */
@ChannelHandler.Sharable
public class DiscardoutBoundHandler extends ChannelOutboundHandlerAdapter{

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        //释放资源
        ReferenceCountUtil.release(msg);
        //通知ChannelPromise数据已经被处理了
        promise.setSuccess();
    }
}
