package com.awakening.ChannelHandler;

import io.netty.channel.ChannelHandlerAdapter;
import io.netty.channel.ChannelHandlerContext;

/**
 * @Author:AwakeningCode
 * @Date: Created in 16:22 2018\3\6 0006
 */
public class WriteHandler extends ChannelHandlerAdapter{

    private ChannelHandlerContext ctx;

    @Override
    public void handlerAdded(ChannelHandlerContext ctx) throws Exception {
        //存储到ChannelHandlerContext的引用以供稍后使用
        this.ctx = ctx;
    }

    public void send(String msg){
        //使用之前存储的到ChannelHandlerContext的引用来发送消息
        ctx.writeAndFlush(msg);
    }
}
