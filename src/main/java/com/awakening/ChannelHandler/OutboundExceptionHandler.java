package com.awakening.ChannelHandler;

import io.netty.channel.*;

/**
 * @Author:AwakeningCode
 * @Date: Created in 16:58 2018\3\6 0006
 */
public class OutboundExceptionHandler extends ChannelOutboundHandlerAdapter{

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        promise.addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture f) throws Exception {
                if (!f.isSuccess()){
                    f.cause().printStackTrace();
                    f.channel().close();
                }
            }
        });
    }
}
