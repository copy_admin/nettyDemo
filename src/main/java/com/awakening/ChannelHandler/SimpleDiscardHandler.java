package com.awakening.ChannelHandler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @Author:AwakeningCode
 * @Date: Created in 14:37 2018\3\6 0006
 */
@ChannelHandler.Sharable
//扩展了SimpleChannelInboundHandler
public class SimpleDiscardHandler extends SimpleChannelInboundHandler<Object>{

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object o) throws Exception {
        //不需要任何显式的资源释放
        //No need to do anything special
    }
}
