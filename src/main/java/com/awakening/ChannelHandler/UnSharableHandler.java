package com.awakening.ChannelHandler;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @Author:AwakeningCode
 * @Date: Created in 16:31 2018\3\6 0006
 */
@ChannelHandler.Sharable
public class UnSharableHandler extends ChannelInboundHandlerAdapter{

    private int count;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        //将count字段值加1
        count++;
        System.out.println("channelRead(...) called the " + count + " time");
        //记录方法调用，并转发给下一个ChannelHandler
        ctx.fireChannelRead(msg);
    }
}
