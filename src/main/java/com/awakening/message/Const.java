package com.awakening.message;

import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author:AwakeningCode
 * @Date: Created in 11:29 2018\2\5 0005
 */
public class Const {

    private static final Logger log = LoggerFactory.getLogger(Const.class);

    private static Map<String,Channel> map = new ConcurrentHashMap<String, Channel>();
    public static void add(String clientId,Channel channel){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        log.info("add ChannelId: " + clientId + " " + formatter.format(new Date()));
        map.put(clientId,channel);
    }
    public static Channel get(String clientId){
        return map.get(clientId);
    }
    public static void remove(Channel channel){
        for (Map.Entry entry:map.entrySet()){
            if (entry.getValue()==channel){
                map.remove(entry.getKey());
            }
        }
    }
    public static int getSzie(){
        return map.size();
    }
    public static void mapInfo(){
        log.info("channel size : " + map.size());
        for (Map.Entry entry:map.entrySet()){
            log.info("clientId: " + entry.getKey());
        }
    }

    public enum StateEnum{
        SUCCESS(1,(byte) 0x01);
        private int state;

        private byte stateInfo;

        StateEnum(int state, byte stateInfo) {
            this.state = state;
            this.stateInfo = stateInfo;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }

        public byte getStateInfo() {
            return stateInfo;
        }

        public void setStateInfo(byte stateInfo) {
            this.stateInfo = stateInfo;
        }

        public static StateEnum stateOf(int index){
            for (StateEnum item:values()){
                if (item.getState() == index){
                    return item;
                }
            }
            return null;
        }
    }

}
