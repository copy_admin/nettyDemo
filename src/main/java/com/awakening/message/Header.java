package com.awakening.message;

import java.nio.ByteBuffer;

/**
 * 消息头部类
 * @Author:AwakeningCode
 * @Date: Created in 18:48 2018\2\4 0004
 */
public class Header {

    private int msgLength;//整个数据的长度包括头部

    private byte msgType;//消息类型的一个字节

    public int getMsgLength() {
        return msgLength;
    }

    public void setMsgLength(int msgLength) {
        this.msgLength = msgLength;
    }

    public byte getMsgType() {
        return msgType;
    }

    public void setMsgType(byte msgType) {
        this.msgType = msgType;
    }

    public byte[] getBytes() {
        byte[] buffer = new byte[5];
        ByteBuffer b = ByteBuffer.allocate(4);
        b.putInt(msgLength);
        System.arraycopy(b.array(), 0, buffer, 0, 4);
        buffer[4] = msgType;
        return buffer;
    }
}
