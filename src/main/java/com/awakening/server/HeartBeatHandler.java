package com.awakening.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * 心跳包
 * @Author:AwakeningCode
 * @Date: Created in 19:43 2018\2\4 0004
 */
public class HeartBeatHandler extends ChannelInboundHandlerAdapter{

    private static final Logger log = LoggerFactory.getLogger(HeartBeatHandler.class);

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        if (evt instanceof IdleStateEvent){
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state() == IdleState.READER_IDLE){
                log.info("READER_IDLE: read timeout from " + ctx.channel().remoteAddress());
            }
        }
    }
}
