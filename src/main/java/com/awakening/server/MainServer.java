package com.awakening.server;

/**
 * @Author:AwakeningCode
 * @Date: Created in 20:17 2018\2\4 0004
 */
public class MainServer {
    public static void main(String[] args) {
        NettyServer server = new NettyServer();
        server.doStart();
    }
}
