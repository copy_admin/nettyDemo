package com.awakening.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * @Author:AwakeningCode
 * @Date: Created in 19:53 2018\2\4 0004
 */
public class NettyServerChannelInitializer extends ChannelInitializer<SocketChannel>{

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        socketChannel.pipeline().addLast(new IdleStateHandler(3*60,0,0));
        socketChannel.pipeline().addLast(new HeartBeatHandler());
        socketChannel.pipeline().addLast(new LengthFieldBasedFrameDecoder(65536,0,4,-4,0));
        socketChannel.pipeline().addLast(new ToMessageDecoder());
        socketChannel.pipeline().addLast(new ServerHandler());
    }
}
