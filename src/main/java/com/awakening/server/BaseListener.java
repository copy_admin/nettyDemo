package com.awakening.server;

import com.awakening.message.Const;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;

/**
 * @Author:AwakeningCode
 * @Date: Created in 17:11 2018\2\5 0005
 */
public class BaseListener implements MessageListener{

    private static final Logger log = LoggerFactory.getLogger(BaseListener.class);

    public BaseListener() {
    }

    @Override
    public void onMessage(Message message) {
        log.debug("onMessage from Server Testing");
        // do some thing with the message
        if (message != null) {
            Const.mapInfo();
            for (Channel c : ServerHandler.channels) {
                ByteBuf msg = Unpooled.copiedBuffer(message.getBody());
                c.writeAndFlush(msg);
            }
        }
    }

    public void onMessageToOne(Message message,int clientId){
        if (message != null && Const.get(String.valueOf(clientId))!=null) {
            log.info("onMessageToOne from Server Testing");
            Channel c = Const.get(String.valueOf(clientId));
            ByteBuf msg = Unpooled.copiedBuffer(message.getBody());
            c.writeAndFlush(msg);
        }
    }
}
