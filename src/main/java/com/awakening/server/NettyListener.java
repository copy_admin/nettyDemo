package com.awakening.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;

import java.util.concurrent.TimeUnit;

/**
 * @Author:AwakeningCode
 * @Date: Created in 17:03 2018\2\5 0005
 */
public class NettyListener {

    private static final Logger log = LoggerFactory.getLogger(NettyListener.class);

    private Thread listenThread;

    public void start(){
        listenThread = new Thread(){
            @Override
            public void run() {
                try {
                    byte[] bodyTesting = {0x11,0x22};
                    Message message = new Message(bodyTesting,null);
                    BaseListener listener = new BaseListener();
                    while (true){
                        log.debug("listener start onMessage");
                        listener.onMessage(message);
                        TimeUnit.SECONDS.sleep(5);
                        listener.onMessageToOne(message,2);
                    }
                }catch (Exception ex){
                    log.error(String.format(
                            "Create Netty listener error %s",
                            ex.getMessage()));
                }
            }
        };
        listenThread.setDaemon(true);
        listenThread.start();
    }

}
