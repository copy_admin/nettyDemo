package com.awakening.EventLoop;

import io.netty.channel.Channel;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * @Author:AwakeningCode
 * @Date: Created in 10:02 2018\3\7 0007
 */
public class Test {

    public static void main(String[] args) {

        /*while(!terminated){
            //阻塞，直到有事件已经就绪可被运行
            List<Runnable> readyEvents = blockUntilEventReady();
            for (Runnable ev:readyEvents){
                //循环遍历，并处理所有的事件
                ev.run();
            }
        }*/

        /*//创建一个其线程池具有10个线程的ScheduledExecutorService
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(10);
        //创建一个Runnable，以供调度稍后执行
        ScheduledFuture<?> future = executorService.schedule(new Runnable() {
            @Override
            public void run() {
                //该任务要打印的消息
                System.out.println("60 seconds later");
            }
            //调度任务在从现在开始的60秒之后执行
        },60, TimeUnit.SECONDS);
        ...
        //一旦调度任务执行完成，就关闭ScheduledExecutorService以释放资源
        executorService.shutdown();*/

        /*Channel ch = ...;
        ScheduledFuture<?> future = ch.eventLoop().schedule(
                //创建一个Runnable以供调度稍后执行
                new Runnable() {
                    @Override
                    public void run() {
                        //要执行的代码
                        System.out.println("60 seconds later");
                    }
                    //调度任务在从现在开始的60秒之后执行
                },60,TimeUnit.SECONDS
        );*/

        /*Channel ch = ...;
        ScheduledFuture<?> future = ch.eventLoop().scheduleAtFixedRate(
                //创建一个Runnable以供调度稍后执行
                new Runnable() {
                    @Override
                    public void run() {
                        //这将一直运行，直到ScheduledFuture被取消
                        System.out.println("60 seconds later");
                    }
                    //调度在60秒之后，并且以后每隔60秒运行
                },60,60,TimeUnit.SECONDS
        );*/

       /* Channel ch = ...;
        //调度任务，并获得所返回的ScheduledFuture
        ScheduledFuture<?> future = ch.eventLoop().scheduleAtFixedRate(...);
        //some other code that runs...
        boolean mayInterruptIfRunning = false;
        //取消该任务，防止它再次运行
        future.cancel(mayInterruptIfRunning);*/
    }

}
