package com.awakening.bytebuf;

import io.netty.buffer.*;
import io.netty.channel.ChannelHandlerContext;

import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.charset.Charset;

/**
 * @Date: Created in 16:09 2018\3\2 0002
 * @Author:AwakeningCode
 */
public class Test {

    public static void main(String[] args) {
        /*ByteBuf directBuf = ...;
        //检查ByteBuf是否由数组支撑。如果不是，则这是一个直接缓冲区
        if (!directBuf.hasArray()){
            //获取可读字节数
            int length = directBuf.readableBytes();
            //分配一个新的数组来保存具有该长度的字节数据
            byte[] array = new byte[length];
            //将字节复制到该数组
            directBuf.getBytes(directBuf.readerIndex(),array);
            //使用数组、偏移量和长度作为参数调用你的方法
            handleArray(array, 0 ,length);
        }*/

        /*ByteBuf heapBuf = ...;
        //检查ByteBuf是否有一个支撑数组
        //当hasArray（）方法返回false时，尝试访问支撑数组将触发一个UnsupportedOperationException
        //这个模式类似于JDK的ByteBuffer的用法
        if (heapBuf.hasArray()){
            //如果有，则获取对该数组的引用
            byte[] array = heapBuf.array();
            //计算第一个字节的偏移量
            int offset = heapBuf.arrayOffset() + heapBuf.readerIndex();
            //获得可读字节数
            int length = heapBuf.readableBytes();
            //使用数组、偏移量和长度作为参数调用你的方法
            handleArray(array,offset,length);
        }*/

        /*//Use an array to hold message parts
        ByteBuffer[] message = new ByteBuffer[]{header,body};
        //Create a new ByteBuffer and use copy to merge the header and body
        ByteBuffer message2 = ByteBuffer.allocate(header.remaining() + body.remaining());
        message2.put(header);
        message2.put(body);
        message2.flip();*/

        /*CompositeByteBuf messageBuf = Unpooled.compositeBuffer();
        ByteBuf headerBuf = ...;//can be backing or direct
        ByteBuf bodyBuf = ...;//can be backing or direct
        //将ByteBuf实例追加到CompositeByteBuf
        messageBuf.addComponents(headerBuf,bodyBuf);
        ......
        //删除位于索引位置为0（第一个组件）的ByteBuf
        messageBuf.removeComponent(0);//remove the header
        //循环遍历所有的ByteBuf实例
        for(ByteBuf buf: messageBuf){
            System.out.println(buf.toString());
        }*/

        /*CompositeByteBuf compBuf = Unpooled.compositeBuffer();
        //获得可读字节数
        int length = compBuf.readableBytes();
        //分配一个具有可读字节数长度的新数组
        byte[] array = new byte[length];
        //将字节读到该数组中
        compBuf.getBytes(compBuf.readerIndex(),array);
        //使用偏移量和长度作为参数使用该数组
        handleArray(array,0,array.length);*/

        /*ByteBuf buffer = ...;
        for (int i = 0; i < buffer.capacity(); i++){
            byte b = buffer.getByte(i);
            System.out.println((char)b);
        }*/

        /*ByteBuf buffer = ...;
        while (buffer.isReadable()){
            System.out.println(buffer.readByte());
        }*/

       /* //Fills the writable bytes of a buffer with random integers
        ByteBuf buffer = ...;
        while (buffer.writableBytes() >= 4){
            buffer.writeInt(random.nextInt());
        }*/

       /*ByteBuf buffer = ...;
       int index = buffer.forEachByte(ByteBufProcessor.FIND_CR);*/


       /*Charset utf8 = Charset.forName("UTF-8");
        //创建一个用于保存给定字符串的字节的ByteBuf
       ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!",utf8);
       //创建该ByteBuf从索引0开始到索引15结束的一个新切片
        ByteBuf sliced = buf.slice(0,15);
        //打印内容
        System.out.println(sliced.toString(utf8));
        //更新索引0处的字节
        buf.setByte(0,(byte)'J');
        //将会成功，因为数据是共享的，对其中一个所做的更改对另一个也是可见的
        assert buf.getByte(0) == sliced.getByte(0);*/

       /*Charset utf8 = Charset.forName("UTF-8");
       //创建ByteBuf以保存所提供的字符串的字节
       ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!",utf8);
       //创建该ByteBuf从索引0开始到索引15结束的分段的副本
       ByteBuf copy = buf.copy(0,15);
       //打印内容
        System.out.println(copy.toString(utf8));
        //更新索引0处的字节
        buf.setByte(0,(byte)'J');
        //将会成功，因为数据不是共享的
        assert buf.getByte(0) != copy.getByte(0);*/


       /*Charset utf8 = Charset.forName("UTF-8");
       //创建一个新的ByteBuf以保存给定字符串的字节
       ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!",utf8);
       //打印第一个字符‘N’
        System.out.println((char)buf.getByte(0));
        //存储当前的readIndex和writeIndex
        int readerIndex = buf.readerIndex();
        int writeIndex = buf.writerIndex();
        //将索引0处的字节更新为字符‘B’
        buf.setByte(0,(byte)'B');
        //打印第一个字符，现在是‘B’
        System.out.println((char)buf.getByte(0));
        //将会成功，因为这些操作并不会修改相应的索引
        assert readerIndex == buf.readerIndex();
        assert writeIndex == buf.writerIndex();*/

        /*Charset utf8 = Charset.forName("UTF-8");
        //创建一个新的ByteBuf以保存给定字符串的字节
        ByteBuf buf = Unpooled.copiedBuffer("Netty in Action rocks!",utf8);
        //打印字符‘N’
        System.out.println((char)buf.readByte());
        //存储当前的readerIndex
        int readerIndex = buf.readerIndex();
        //存储当前的writeIndex
        int writeIdnex = buf.writerIndex();
        //将字符‘？’追加到缓冲区
        buf.writeByte((byte)'?');
        assert readerIndex == buf.readerIndex();
        //将会成功，因为writeByte（）方法移动了writeIndex
        assert writeIdnex != buf.writerIndex();*/

        /*io.netty.channel.Channel channel = ...;
        ByteBufAllocator allocator = channel.alloc();
        .....
        ChannelHandlerContext ctx = ...;
        ByteBufAllocator allocator1 = ctx.alloc();
        ....*/

        /*io.netty.channel.Channel channel = ...;
        //从Channel获取ByteBufAllocator
        ByteBufAllocator allocator = channel.alloc();
        ...
        //从ByteBufAllocator分配一个ByteBuf
        ByteBuf buffer = allocator.directBuffer();
        //检查引用技术是否为预期的1
        assert buffer.refCnt() == 1;*/

        /*ByteBuf buffer = ...;
        //减少到该对象的活动引用。当减少到0时，该对象被释放，并且该方法返回true
        boolean released = buffer.release();*/

    }

}
