package com.awakening.preset;

import io.netty.buffer.ByteBuf;
import io.netty.channel.*;

/**
 * @Author:AwakeningCode
 * @Date: Created in 14:05 2018\3\9 0009
 */
public class LengthFieldBasedFrameDecoder extends ChannelInitializer<Channel>{
    @Override
    protected void initChannel(Channel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();
        pipeline.addLast(new io.netty.handler.codec.LengthFieldBasedFrameDecoder(64*1024,0,8));
        //添加FrameHandler以处理每个帧
        pipeline.addLast(new FrameHandler());
    }
    public static final class FrameHandler extends SimpleChannelInboundHandler<ByteBuf>{
        @Override
        protected void channelRead0(ChannelHandlerContext channelHandlerContext,
                                    ByteBuf byteBuf) throws Exception {
            //处理帧的数据
            // Do something with the frame
        }
    }
}
