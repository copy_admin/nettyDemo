package com.awakening.preset;

import io.netty.channel.*;

import java.io.File;
import java.io.FileInputStream;

/**
 * @Author:AwakeningCode
 * @Date: Created in 14:16 2018\3\9 0009
 */
public class Test {

    public static void main(String[] args) throws Exception{
        Channel channel = null;
        File file = null;
        //创建一个FileInputStream
        FileInputStream in = new FileInputStream(file);
        FileRegion region = new DefaultFileRegion(
                in.getChannel(),0,file.length());
        //发送该DefaultFileRegion，并注册一个ChannelFutureListener
        channel.writeAndFlush(region).addListener(
                new ChannelFutureListener() {
                    @Override
                    public void operationComplete(ChannelFuture channelFuture) throws Exception {
                        if (!channelFuture.isSuccess()) {
                            //处理失败
                            Throwable cause = channelFuture.cause();
                            // Do something
                        }
                    }
                }
        );
    }

}
