package com.awakening.EmbeddedChannel;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author:AwakeningCode
 * @Date: Created in 14:30 2018\3\8 0008
 */
public class AbsIntegerEncoderTest {
    @Test
    public void encode() throws Exception {
        //创建一个ByteBuf，并且写入9个负整数
        ByteBuf buf = Unpooled.buffer();
        for (int i = 1; i < 10; i++){
            buf.writeInt(i * -1);
        }
        //创建一个EmbeddedChannel，并安装一个要测试的AbsIntegerEncoder
        EmbeddedChannel channel = new EmbeddedChannel(
                new AbsIntegerEncoder());
        //写入ByteBuf，并断言调用readOutbound（）方法将会产生数据
        assertTrue(channel.writeOutbound(buf));
        assertTrue(channel.finish());

        //读取所产生的消息，并断言它们包含了对应的绝对值
        //read bytes
        for (int i=1; i < 10; i++){
            assertEquals(i , channel.readOutbound());
        }
        assertNull(channel.readOutbound());
    }

}