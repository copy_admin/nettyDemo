package com.awakening.EmbeddedChannel;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.embedded.EmbeddedChannel;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author:AwakeningCode
 * @Date: Created in 14:01 2018\3\8 0008
 */
public class FixedLengthFrameDecoderTest {
    @Test
    public void decode() throws Exception {
        //创建一个ByteBuf，并存储9个字节
        ByteBuf buf = Unpooled.buffer();
        for (int i = 0; i < 9; i++){
            buf.writeByte(i);
        }

        ByteBuf input = buf.duplicate();
        //创建一个EmbeddedChannel，并添加一个FixedLengthFrameDecoder，其将以3字节的帧长度被测试
        EmbeddedChannel channel = new EmbeddedChannel(new FixedLengthFrameDecoder(3));
        //write bytes
        //将数据写入EmbeddedChannel
        assertTrue(channel.writeInbound(input.retain()));
        //标记Channel为已完成状态
        assertTrue(channel.finish());

        //read messages
        //读取所生成的消息，并且验证是否有3帧，其中每帧都为3字节
        ByteBuf read = (ByteBuf)channel.readInbound();
        assertEquals(buf.readSlice(3),read);
        read.release();

        assertNull(channel.readInbound());
        buf.release();
    }

}